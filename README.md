I tried to implemented fundamental features you asked for. I believe you will also notice some places are there to improve. Because of the lack of time or busy schedules I have intentionally avoided some of the tools you mentioned which I personally use in my regular projects such as: Flow or TypeScript

Since it's a prototype project I used create-react-app for faster prototyping.

## Technology USED
    - Apollo
    - Redux 
    - React (16.7.0)
    - React-Router
    - Webpack
    -

## Features Implemented
    - Fetching issues from github using graphql API for facebook/react repository
    - Showing List of issues
    - Showing detail view of an Issue with all the comments
    - Filter issues based on text search in body, title and open or closed status

## Features in Progress
    - Pagination
    - Implementation of Typescript or Flow
