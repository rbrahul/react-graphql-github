import * as ACTION_TYPES from './../constants/actionTypes';

const initialState = {
    searchQuery: 'is:issue repo:facebook/react',
};

export default function(state = initialState, action) {
    switch (action.type) {
        case ACTION_TYPES.SET_SEARCH_QUERY:
            return {
                searchQuery: `${action.data} ${initialState.searchQuery}` 
            };

        case ACTION_TYPES.RESET_SEARCH_QUERY:
            return {
                searchQuery: initialState.searchQuery 
            };
        default:
            return state;
    }
}
