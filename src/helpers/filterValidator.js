const getValidFlags = word => {
    const matched = /(\w+)(:)(\w+)/.exec(word.toLowerCase());
    return {
        prefix: matched[1],
        sufix: matched[3],
    }
};

const acceptedPattern = {
    is: ['closed', 'open'],
    'in': ['body', 'title'],
};


export const getValidationErrors  = text => {
    const keywords = text.split(' ');
    let errors = [];
    keywords.forEach(word => {
        if (/(\w+)(:)(\w+)/.test(word)) {
            const pattern = getValidFlags(word);
            if (!acceptedPattern[pattern.prefix]) {
                const examplePatterns = Object.keys(acceptedPattern)
                .join(', ');
                errors = [...errors, `${pattern.prefix} is not allowed as query prefix. Accepted keywords are ${examplePatterns}`];
            }
            else if (!acceptedPattern[pattern.prefix].includes(pattern.sufix)) {
                const examplePatterns = acceptedPattern[pattern.prefix]
                    .map(item => `${pattern.prefix}:${item}`)
                    .join(', ');
                    errors = [...errors, `${pattern.sufix} is not allowed as query suffix. Accepted keywords are ${examplePatterns}`];
            }
        }
    });
    return errors.length ? errors : false;
}

