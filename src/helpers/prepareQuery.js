export const getSearchableQuery = text => {
    let searchText = text.trim();
    const keywords = searchText.split(' ');
    const queryKeyWords = []
    keywords.forEach(word => {
        if (/(\w+)(:)(\w+)/.test(word)) {
            queryKeyWords.push(word);
            searchText = searchText.replace(/(\w+)(:)(\w+)/, '');
        }
    });
    const searchByTextIn = queryKeyWords.filter(word => word.startsWith('in'));
    const keywordsWithTextSearch = queryKeyWords.filter(word => !word.startsWith('in'));
    let searchQuery = '';
    if (searchByTextIn.length > 0 && searchText.length) {
        searchQuery = `${searchText} ${searchByTextIn[searchByTextIn.length - 1]} `;
    } else if(searchText.length) {
        searchQuery = `${searchText} in:title `; 
    }
    return (searchQuery += keywordsWithTextSearch.join(' '));
}

