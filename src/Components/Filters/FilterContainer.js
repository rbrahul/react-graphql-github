import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Filter from './Filters';
import {
    setSearchQuery,
    resetSearchQuery
} from './../../actions/issues';

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            setSearchQuery,
            resetSearchQuery
        },
        dispatch);
};

export default connect(null, mapDispatchToProps)(Filter);
