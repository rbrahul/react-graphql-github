import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Button,
    Chip,
    FormControl,
    Grid,
    Input,
    InputAdornment,
    InputLabel,
} from '@material-ui/core';
import {
    Search,
    Close,
} from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
import { getValidationErrors } from './../../helpers/filterValidator';
import { getSearchableQuery } from './../../helpers/prepareQuery';

const styles = theme => ({
    filterBar: {
        display: 'flex',
        flexWrap: 'wrap',
        padding: `${theme.spacing.unit * 2}px`
    },
    errorMessage: {
        display: 'flex',
        flexWrap: 'wrap',
        padding: `${theme.spacing.unit}px`,
    },
    clearBtn: {
        marginTop: `20px`
    }
});

class Filters extends Component {
    constructor(props) {
        super(props);
        this.filterTextField = React.createRef();
        this.state = {
            queryText: '',
            validationErrors: []
        }
    }

    filterByQuery = event => {
        const queryText = event.target.value;
            this.setState({
                queryText
            });
    }

    resetFilter = () => {
        this.setState({
            queryText: '',
            validationErrors: []
        });
        this.props.resetSearchQuery();

    }

    searchWithQuery = event => {
        event.preventDefault();
        this.setState({ validationErrors: [] });
        const validationErrors = getValidationErrors(this.state.queryText.trim());
        if (validationErrors) {
            this.setState({ validationErrors });
        } else {
            const query = getSearchableQuery(this.state.queryText);
            this.props.setSearchQuery(query);
        }
    }

    render() {
        const { classes } = this.props;
        return (<div className={classes.filterBar}>
            <Grid container spacing={32} xs={12}>
                <Grid item xs={6}>
                    <form onSubmit={this.searchWithQuery}>
                        <FormControl fullWidth className={classes.formControl}>
                            <InputLabel htmlFor='component-simple'>Filter by text</InputLabel>
                            <Input
                                ref={this.filterTextField}
                                onChange={this.filterByQuery}
                                value={this.state.queryText}
                                startAdornment={
                                    <InputAdornment position='start'>
                                        <Search />
                                    </InputAdornment>
                                } />
                        </FormControl>
                    </form>
                </Grid>
                {this.state.queryText && <Grid xs={4}>
                    <FormControl>
                        <Button onClick={this.resetFilter} color="secondary" className={classes.clearBtn}> <Close fontSize="large" /> Clear Filtering </Button>
                    </FormControl>
                </Grid>}
                {this.state.validationErrors.length > 0 && <Grid xs={12} className={classes.errorMessage}>
                    {
                        this.state.validationErrors.map(errorItem => {
                            return <Chip label={errorItem} className={classes.chip} variant="outlined" color="secondary" />
                        })
                    }
                </Grid>}

            </Grid>
        </div>)
    }
}

Filters.propTypes = {
    classes: PropTypes.object.isRequired,
    resetSearchQuery: PropTypes.func.isRequired,
    setSearchQuery: PropTypes.func.isRequired,
};

export default withStyles(styles)(Filters);
