import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import {
    Divider,
    IconButton,
    ListItem,
    ListItemIcon,
    ListItemSecondaryAction,
    ListItemText,
    Typography
} from '@material-ui/core';
import {
    Comment as CommmentIcon,
    Info as InfoIcon,
} from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';


const styles = theme => ({
    issueHeading: {
        textDecoration: 'none'
    }
});

const IssueItem = props => {
    const { classes, id, title, author, commentCount } = props;
    return (<Fragment >
        <Link to={`issues/${id}`} className={classes.issueHeading} >
            <ListItem role={undefined} dense button onClick={() => { }}>
                <ListItemIcon>
                    <InfoIcon color='primary' />
                </ListItemIcon>
                <ListItemText
                    primary={
                        <Typography variant='subtitle2' >{title}</Typography>
                    }
                    secondary={
                        <Typography component='span' className={classes.inline} color='textPrimary'>
                            Created By {author}
                        </Typography>
                    }
                />
                {commentCount > 0 && <ListItemSecondaryAction>
                    <IconButton aria-label='Comments'>
                        <CommmentIcon /> <Typography component='span' className={classes.inline} color='textPrimary'>{commentCount}</Typography>
                    </IconButton>
                </ListItemSecondaryAction>}
            </ListItem>
        </Link>
        <Divider component='li' />
    </Fragment>)
};

IssueItem.propTypes = {
    title: PropTypes.string.isRequired,
    author: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    commentCount: PropTypes.number.isRequired,
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(IssueItem);
