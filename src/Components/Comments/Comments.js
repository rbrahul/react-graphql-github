import React from 'react';
import PropTypes from 'prop-types';
import {
    Avatar,
    Card,
    CardHeader,
    CardContent,
    Typography
} from '@material-ui/core';
import deepPurple from '@material-ui/core/colors/deepPurple';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    card: {
        marginBottom: `${theme.spacing.unit * 3}px`,
        width: '100%'
    },
    avatar: {
        backgroundColor: deepPurple[800],
    }
});

const Comments = props => {
    const { comments, classes } = props;
    return comments.map(
        ({
            bodyHTML,
            author,
            createdAt
        },
            key) => {
            return (<Card className={classes.card} key={key}>
                <CardHeader
                    avatar={
                        <Avatar aria-label="User" src={(author && author.avatarUrl) ? author.avatarUrl : ''} color="" className={classes.avatar} />
                    }
                    title={(author && author.login) ? author.login : 'Anonymous'}
                    subheader={createdAt}
                />
                <CardContent>
                    <Typography variant="subheading" dangerouslySetInnerHTML={{ __html: bodyHTML }} />
                </CardContent>
            </Card>)
        })
};


Comments.propTypes = {
    comments: PropTypes.array.isRequired,
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Comments);
