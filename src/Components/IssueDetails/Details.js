import React from 'react';
import PropTypes from 'prop-types';
import {
    Avatar,
    Card,
    CardHeader,
    Grid,
    CardContent,
    Typography
} from '@material-ui/core';
import deepPurple from '@material-ui/core/colors/deepPurple';
import { withStyles } from '@material-ui/core/styles';
import Comments from './../Comments';


const styles = theme => ({
    card: {
        marginBottom: `${theme.spacing.unit * 3}px`,
        width: '100%'
    },
    avatar: {
        backgroundColor: deepPurple[800],
    }
});



const Details = props => {
    const {
        classes,
        issue: { author, createdAt, bodyHTML, title, comments }
    } = props;
    return (
        <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
        >
            <Card className={classes.card} >
                <CardHeader
                    avatar={
                        <Avatar aria-label="User" src={(author && author.avatarUrl) ? author.avatarUrl : ''} color="" className={classes.avatar} />
                    }
                    title={(author && author.login) ? author.login : 'Anonymous'}
                    subheader={createdAt}
                />
                <CardContent>
                    <Typography variant="h2">{title}</Typography>
                    <Typography variant="subheading" dangerouslySetInnerHTML={{ __html: bodyHTML }} />
                </CardContent>
            </Card>
            <Comments
                comments={comments.nodes}
            />
        </Grid>
    )
};

Details.propTypes = {
    issue: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Details);
