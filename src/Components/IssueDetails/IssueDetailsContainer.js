import React from 'react';
import { Query } from "react-apollo";
import gql from "graphql-tag";
import Details from './Details';

const GET_ISSUES = gql`
query Repository($issueNumber: Int!, $repo: String!){
    repository(name: $repo, owner: "facebook"){
      issue(number: $issueNumber){
      createdAt
      bodyHTML
      title
      author{
        avatarUrl
        login
      }
     	comments(first: 50) {
          nodes {
            author {
              login
              avatarUrl
            }
            createdAt
            bodyHTML
          }
   		 }
      }
  }
}
`;

const IssuesQuery = props => {
    console.log(props);
    return (<Query
        query={GET_ISSUES}
        variables={{ repo: 'react', issueNumber: parseInt(props.match.params.id) || 0 }}
    >
        {({ loading, error, data }) => {
            if (loading) return <p>Loading...</p>;
            if (error) return <p>Error :(</p>;
            return <Details
                issue={data.repository.issue}
            />
        }}
    </Query>
    );
};

export default IssuesQuery;