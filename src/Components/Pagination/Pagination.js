import React from 'react';
import PropTypes from 'prop-types';
import {
    Button
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    btn: {
        marginRight: `${theme.spacing.unit}px`,
    },
});

const Pagination = props => {
    const { classes } = props;
    return (
        <>
            <Button size='small' color='primary' className={classes.btn}>Prev</Button>
            {
                [1, 2, 3, 4, 5].map((value, key) => (<Button
                    key={key}
                    size='small'
                    color='primary'
                    variant='outlined'
                    className={classes.btn}
                >
                    {value}
                </Button>))
            }

            <Button size='small' color='primary'>Next</Button>
        </>);
};

Pagination.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Pagination);
