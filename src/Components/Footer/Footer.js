
import React from 'react';
import PropTypes from 'prop-types';

import {
    Typography
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing.unit * 6,
      }
});

const Footer = props => {
    const { classes } = props;
    return (<footer className={classes.footer}>
        <Typography variant="subtitle1" align="left" color="textSecondary" component="p">
            Developed With Love
        </Typography>
    </footer>)
};

Footer.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Footer);
