import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    Card,
    CardContent,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import Filters from './../Filters';
import IsseuQuery from './issueQuery';


const styles = theme => ({
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%',
    },
    cardContent: {
        flexGrow: 1,
    },
    filterBar: {
        display: 'flex',
        flexWrap: 'wrap',
        padding: `${theme.spacing.unit * 2}px`
    },
});

class Issues extends Component {
    render() {
        const { classes } = this.props;
        return (<Card className={classes.card}>
            <CardContent className={classes.cardContent}>
                <Filters />
                <IsseuQuery />
            </CardContent>
        </Card>)
    }
}

Issues.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Issues);
