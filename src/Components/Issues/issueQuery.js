import React from 'react';
import { connect } from 'react-redux';
import { Query } from "react-apollo";
import gql from "graphql-tag";

import {
    Divider,
    List,
    Typography,
    Grid,
} from '@material-ui/core';
import IssueItem from '../IssueItem';

const GET_ISSUES = gql`
query Repository($searchQuery: String!) {
    search(query: $searchQuery, type: ISSUE, first: 20) {
      issueCount
      nodes {
        __typename
        ... on Issue {
          number
          title
          state
          repository {
            name
          }
          comments {
            totalCount
          }
          author {
            login
          }
        }
      }
    }
  }
`;
const renderIssueList = issues => {
    return issues.map((issue, key) => (
        <IssueItem
            id={issue.number}
            key={key}
            title={issue.title}
            commentCount={(issue.comments || {}).totalCount}
            author={issue.author && issue.author.login}
        />
    ))
}

const IssuesQuery = ({ searchQuery }) => (
    <Query
        query={GET_ISSUES}
        variables={{ searchQuery }}
    >
        {({ loading, error, data }) => {
            if (loading) return <p>Loading...</p>;
            if (error) return <p>Error :(</p>;
            return (<Grid>
                <Typography gutterBottom variant='h5' component='h2'>
                    {data.search.issueCount} Issues Found
                </Typography>
                <List>
                    <Divider component='li' />
                    {renderIssueList(data.search.nodes)}
                </List>
            </Grid>)
        }}
    </Query>
);

const mapStateToProps = state => {
    return {
        searchQuery: state.issues.searchQuery
    };
}


export default connect(mapStateToProps)(IssuesQuery);