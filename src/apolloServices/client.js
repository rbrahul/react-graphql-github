import ApolloClient, { InMemoryCache } from 'apollo-boost';


//FIXME: UNSAFE STORING ACCESS TOKEN - SECURITY ISSUE
const token = '30f0d04fd3586e068660cceb1247ffc95dd04540';

const cache = new InMemoryCache();

export default new ApolloClient({
  uri: 'https://api.github.com/graphql',
  headers: {
    authorization: token ? `Bearer ${token}` : '',
  },
  cache
});
