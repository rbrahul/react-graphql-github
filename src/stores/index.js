import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers';

const middleware = [thunk];
let extension = next => next;
extension = window.devToolsExtension ? window.devToolsExtension() : extension;
 
const store = createStore(
    rootReducer,
    compose(applyMiddleware(...middleware), extension)
);

export default store;
