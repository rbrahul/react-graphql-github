import * as ACTION_TYPES from './../constants/actionTypes';

export const setSearchQuery = query => {
    return {
        type: ACTION_TYPES.SET_SEARCH_QUERY,
        data: query
    };
};

export const resetSearchQuery = () => {
    return {
        type: ACTION_TYPES.RESET_SEARCH_QUERY,
    };
};