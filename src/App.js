import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import classNames from 'classnames';
import AppBar from '@material-ui/core/AppBar';
import Camera from '@material-ui/icons/Camera';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Issues from './Components/Issues';
import Footer from './Components/Footer';
import IssueDetails from './Components/IssueDetails';

const styles = theme => ({
  appBar: {
    position: 'relative',
  },
  icon: {
    marginRight: theme.spacing.unit * 2,
  },
  heroUnit: {
    backgroundColor: theme.palette.background.paper,
  },
  heroContent: {
    maxWidth: 1100,
    margin: '0 auto',
    padding: `${theme.spacing.unit * 3}px 0 ${theme.spacing.unit * 3}px`,
  },
  heroButtons: {
    marginTop: theme.spacing.unit * 4,
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1100,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  cardGrid: {
    padding: `${theme.spacing.unit * 8}px 0`,
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardContent: {
    flexGrow: 1,
  },
  

  filterBar: {
    display: 'flex',
    flexWrap: 'wrap',
    padding: `${theme.spacing.unit * 2}px`
  },
});


const App = props => {
  const { classes } = props;
  return (
    <>
      <CssBaseline />
      <AppBar position="static" className={classes.appBar}>
        <Toolbar>
          <Camera className={classes.icon} />
          <Typography variant="h6" color="inherit" noWrap>
            React Issues
          </Typography>
        </Toolbar>
      </AppBar>
      <main>
        <div className={classes.heroUnit}>
          <div className={classes.heroContent}>
            <Typography component="h1" variant="h2" align="left" color="textPrimary" gutterBottom>
              React
            </Typography>
            <Typography variant="h6" align="left" color="textSecondary" paragraph>
              A declarative, efficient, and flexible JavaScript library for building user interfaces
            </Typography>

          </div>
        </div>
        <div className={classNames(classes.layout, classes.cardGrid)}>
        <Switch>
          <Route path='/' exact component={Issues} />
          <Route path='/issues/:id' component={IssueDetails} />
        </Switch>
        </div>
      </main>
      <Footer />
    </>
  );
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);